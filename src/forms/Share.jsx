import IdentityShareForm from '../modules/identity/IdentityShareForm'

// allows identity share form to be accessed using the URL param ?form=ShareIdentity
export default IdentityShareForm