import IdentityRequestForm from '../modules/identity/IdentityRequestForm'

// allows identity request form to be accessed using the URL param ?form=ShareRequest
export default IdentityRequestForm