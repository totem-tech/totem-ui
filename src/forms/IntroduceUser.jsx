import IntroduceUserForm from '../modules/identity/IntroduceUserForm'

// allows identity share form to be accessed using the URL param ?form=ShareIdentity
export default IntroduceUserForm